namespace ns {
  template<typename T>
  class Foo {
  public:
    Foo();
  };
  using Bar = Foo<int>;
}

template<>
ns::Foo<int>::Foo()
{
}

int main()
{
  ns::Bar lol;
}
